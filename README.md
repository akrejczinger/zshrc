# ZSH config files

Installation:

- Check antibody installation instructions at
  the [antibody page](http://getantibody.github.io/)
- Run the following commands:
  ```zsh
  homesick clone akrejczinger/zshrc
  homesick symlink zshrc
  ```
