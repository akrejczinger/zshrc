#!/usr/bin/env zsh

## General
# Newer ubuntu version doesn't always have this defined
export XDG_CONFIG_HOME=$HOME/.config
# Add custom scripts and python dir to PATH
export PATH=~/.local/scripts:~/.local/bin:~/.gem/ruby/2.6.0/bin:~/.cargo/bin:~/.poetry/bin:$PATH
export EDITOR="nvim"
export PAGER="less"

## Load plugins
source <(antibody init)
antibody bundle < $HOME/zsh_plugins.txt

# Command not found package (only on Ubuntu)
[[ -e /etc/zsh_command_not_found ]] && source /etc/zsh_command_not_found

# Enable direnv
if which direnv; then
    eval "$(direnv hook zsh)"
fi

function is_git_dir() {
    if $(git rev-parse --is-inside-work-tree 2> /dev/null)
    then
        true
    else
        false
    fi
}

# This can be called within the custom prompt to show the number of saved git stashes
function prompt_stash_count() {
    if ! is_git_dir
    then
        return
    fi
    stash_count=$(git stash list | wc -l)
    if [ $stash_count != "0" ]
    then
        echo " (stash $stash_count)"
    fi
}

# Prompt setup
AGKOZAK_CUSTOM_PROMPT='%(?..%B%F{red}(%?%)%f%b )'
AGKOZAK_CUSTOM_PROMPT+='%(!.%S%B.%B%F{green})%n%1v%(!.%b%s.%f%b) '
AGKOZAK_CUSTOM_PROMPT+=$'%B%F{blue}%2v%f%b%(3V.%F{yellow}%3v%f.)\n'
AGKOZAK_CUSTOM_PROMPT+='$(_agkozak_vi_mode_indicator) '
AGKOZAK_CUSTOM_RPROMPT=''

if [ "$TERM" = "linux" ]; then
    echo -en "\e]P0232323" #black
    echo -en "\e]P82B2B2B" #darkgrey
    echo -en "\e]P1D75F5F" #darkred
    echo -en "\e]P9E33636" #red
    echo -en "\e]P287AF5F" #darkgreen
    echo -en "\e]PA98E34D" #green
    echo -en "\e]P3D7AF87" #brown
    echo -en "\e]PBFFD75F" #yellow
    echo -en "\e]P48787AF" #darkblue
    echo -en "\e]PC7373C9" #blue
    echo -en "\e]P5BD53A5" #darkmagenta
    echo -en "\e]PDD633B2" #magenta
    echo -en "\e]P65FAFAF" #darkcyan
    echo -en "\e]PE44C9C9" #cyan
    echo -en "\e]P7E5E5E5" #lightgrey
    echo -en "\e]PFFFFFFF" #white
fi

## Set options
setopt interactivecomments  # make the # character comment in interactive mode too
setopt correct            # Auto correct mistakes
setopt extendedglob       # Extended globbing
setopt nocaseglob         # Case insensitive globbing
# FIXME: rcexpandparam breaks zplug rendering
# setopt rcexpandparam    # Array expension with parameters
setopt nocheckjobs        # Dont warn about running processes when exiting
setopt numericglobsort    # Sort filenames numerically when it makes sense
setopt nobeep             # No beep
setopt histignorealldups  # If a new command is a duplicate, remove the older one
setopt histignorespace    # Whitespace is ignored in command diff
setopt appendhistory      # Append history to history file (no overwriting)
setopt sharehistory       # Share history between multiple sessions
setopt incappendhistory   # Append history immediately, not only on term exit

# Case insensitive tab completion
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'
# Colored completion (different colors for dirs/files/etc)
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"

HISTFILE=~/.zhistory
HISTSIZE=10000
SAVEHIST=10000


## Bindings
# Insert mode bindings
bindkey -e
bindkey '^[[7~' beginning-of-line                   # Home key
bindkey '^[[8~' end-of-line                         # End key
bindkey '^[[2~' overwrite-mode                      # Insert key
bindkey '^[[3~' delete-char                         # Delete key
bindkey '^[[A'  up-line-or-history                  # Up key
bindkey '^[[B'  down-line-or-history                # Down key
bindkey '^[[C'  forward-char                        # Right key
bindkey '^[[D'  backward-char                       # Left key
bindkey '^[[5~' history-beginning-search-backward   # Page up key
bindkey '^[[6~' history-beginning-search-forward    # Page down key


## Vi mode plugin copied from oh-my-zsh
# Updates editor information when the keymap changes.
function zle-keymap-select() {
  zle reset-prompt
  zle -R
}

# Ensure that the prompt is redrawn when the terminal size changes.
TRAPWINCH() {
  zle &&  zle -R
}

zle -N zle-keymap-select
zle -N edit-command-line


bindkey -v

# allow v to edit the command line (standard behaviour)
autoload -Uz edit-command-line
bindkey -M vicmd 'v' edit-command-line

# allow ctrl-p, ctrl-n for navigate history (standard behaviour)
bindkey '^P' up-history
bindkey '^N' down-history

# allow ctrl-h, ctrl-w, ctrl-? for char and word deletion (standard behaviour)
bindkey '^?' backward-delete-char
bindkey '^h' backward-delete-char
bindkey '^w' backward-kill-word

# allow ctrl-r to perform backward search in history
bindkey '^r' history-incremental-search-backward

# allow ctrl-a and ctrl-e to move to beginning/end of line
bindkey '^a' beginning-of-line
bindkey '^e' end-of-line

# if mode indicator wasn't setup by theme, define default
if [[ "$MODE_INDICATOR" == "" ]]; then
  MODE_INDICATOR="%{$fg_bold[red]%}<%{$fg[red]%}<<%{$reset_color%}"
fi

function vi_mode_prompt_info() {
  echo "${${KEYMAP/vicmd/$MODE_INDICATOR}/(main|viins)/}"
}

# # define right prompt, if it wasn't defined by a theme
# if [[ "$RPS1" == "" && "$RPROMPT" == "" ]]; then
#   RPS1='$(vi_mode_prompt_info)'
# fi

## Custom vi mode bindings
bindkey -M vicmd "h" history-beginning-search-backward
bindkey -M vicmd "k" history-beginning-search-forward
bindkey -M vicmd "j" backward-char
bindkey -M vicmd "l" forward-char

# Search in history with cursor keys
bindkey "^[[A" history-beginning-search-backward
bindkey "^[[B" history-beginning-search-forward


## Custom aliases and functions
alias ls="ls --color"
alias cp="cp -i"
alias df="df -h"
alias free="free -m"
alias tree="tree -C"
alias less='less --RAW-CONTROL-CHARS'
alias grep='grep --color'
alias fd='find . -type d -name'
alias ff='find . -type f -name'
alias r='ranger'
alias nrc='nvim ~/.config/nvim/init.vim'
alias npl='nvim ~/.config/nvim/conf/plugins.vim'
alias naw='nvim ~/.config/awesome/rc.lua'

function rcd {
    ranger $@ --choosedir=/tmp/rangerdir;
    cd `cat /tmp/rangerdir`;
}

# Safer file removal - move to trash (uses scripts in PATH)
alias rm='rmtrash'
alias rmdir='rmdirtrash'

# Neovim aliases
alias vim=nvim
alias n=nvim

# Set up ignored dirs for ag
alias ag='ag --path-to-ignore ~/.agignore --follow'

# Weather forecast
alias wt='curl wttr.in/Budapest'

# ex - archive extractor
# usage: ex <file>
ex() {
    if [ -f $1 ] ; then
        case $1 in
            *.tar.bz2)   tar xjf $1   ;;
            *.tar.gz)    tar xzf $1   ;;
            *.bz2)       bunzip2 $1   ;;
            *.rar)       unrar x $1   ;;
            *.gz)        gunzip $1    ;;
            *.tar)       tar xf $1    ;;
            *.tbz2)      tar xjf $1   ;;
            *.tgz)       tar xzf $1   ;;
            *.zip)       unzip $1     ;;
            *.Z)         uncompress $1;;
            *.7z)        7z x $1      ;;
            *)           echo "'$1' cannot be extracted via ex()" ;;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}

function homesick_status_all() {
    for repo in `homesick list | awk '{print $1}'`; do
        pushd `homesick show_path $repo` && git st && popd > /dev/null
    done
}

function fh() {
    # fh - repeat history
    eval $( ([ -n "$ZSH_NAME" ] && fc -l 1 || history) | fzf +s --tac | sed 's/ *[0-9]* *//')
}

function fshow() {
    # fshow - git commit browser
      git log --graph --color=always \
          --format="%C(auto)%h%d %s %C(black)%C(bold)%cr" "$@" |
      fzf --ansi --no-sort --reverse --tiebreak=index --bind=ctrl-s:toggle-sort \
          --bind "ctrl-f:execute:
                    (grep -o '[a-f0-9]\{7\}' | head -1 |
                    xargs -I % sh -c 'git show --color=always % | less -R') << 'FZF-EOF'
                    {}
FZF-EOF"
}

function fco() {
    # fco - checkout git commit
    local commits commit
    commits=$(git log --pretty=oneline --abbrev-commit --reverse) &&
    commit=$(echo "$commits" | fzf --tac +s +m -e) &&
    git checkout $(echo "$commit" | sed "s/ .*//")
}

function vimgit() {
    # vimgit - open all modified files in git with nvim
    local git_status rootdir files
    rootdir=$(git rev-parse --show-toplevel)
    git_status=$(git status --porcelain 2> /dev/null)
    if [[ $git_status != '' ]]; then
        files=$(echo $git_status | awk '{print $2}' | sed -e "s#^#$rootdir/#")
        echo $files | xargs -d '\n' nvim --
    fi
}

function print_projects {
    # get the known projects and put them in $found_dirs
    # Uses two arrays, defined in zsh_aliases:
    # - $PARENT_DIRS for dirs whose SUBDIRS are projects
    # - $PROJECT_DIRS which is a list of single project directories

    # collect all sub-directories in "PARENT_DIRS"
    found_dirs=()
    for dir in $PARENT_DIRS; do
        for subdir in `\ls -d $dir/*`; do
            # path_str=$(echo $subdir | sed 's|/home/arpadkrejczinger|~|')
            found_dirs+=$subdir
        done
    done

    # add all single directories in "PROJECT_DIRS" to the list
    for dir in $PROJECT_DIRS; do
        # path_str=$(echo $dir | sed 's|/home/arpadkrejczinger|~|')
        found_dirs+=$dir
    done

    printf '%s\n' ${found_dirs#/home/arpadkrejczinger/}
}

function cdlast {
    # cd into last project visited using "projects" function
    # Use "pushd" in case the directory switch was not intended (e.g. after "homesick cd")
    pushd `cat ~/.last_project`
}

function cd_projects {
    # select from the known projects, then cd into it. Saves last visited project to ~/.last_project
    # Input is from `print_projects` function.

    selected=$(print_projects | fzf)
    [ -z $selected ] && return # stop if no selection found

    set -o magicequalsubst
    echo ~/$selected > ~/.last_project
    cdlast
}
alias cdp=cd_projects

# Go to last directory automatically
# cdlast

source ~/.xinitrc

# Load local aliases (not versioned in git)
[ -f ~/.zsh_aliases ] && source ~/.zsh_aliases

# FZF config
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
